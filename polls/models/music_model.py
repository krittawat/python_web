from django.db import models



class album(models.Model):
    id = models.IntegerField(primary_key=True)
    album_name = models.CharField(max_length=30)
    
class music(models.Model):
    id = models.IntegerField(primary_key=True)
    music_name = models.CharField(max_length=3)
    music_datetime = models.DateTimeField(auto_now_add=False, blank=True)
    music_album_FK = models.ForeignKey('album',on_delete=models.CASCADE)
    
    
#    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)

#    coupon_code = models.ForeignKey(Voucher, on_delete=models.CASCADE, related_name='approved_coupon_lines_coupons')
