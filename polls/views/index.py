from .control_template import *
from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader


def index(request):
    header_str = "Hello"
    template = loader.get_template('Home/index.html')
    data_control_template = control_template.control_template_1()
    print(data_control_template)
    context = {
        'var1': header_str,
        'data_control_template':data_control_template
    }
    # return HttpResponse(template.render(context,request)) 
    return render(request, "Home/index.html", context)
