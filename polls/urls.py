#from . import views
from .views import index
from .views import login
from django.conf.urls import url
#from django.urls import path

urlpatterns = [
#    path('', index.index, name='index'),
#    path('login', login.login, name='login'),
#    path('view_b',views.view_b, name='view_b'),
#    path('view_b2',views.view_b2, name='view_b2'),

    url(r'^$', index.index, name='index'),
    url(r'^login$', login.login, name='login'),
    url(r'^login2$', login.login2, name='login2'),
    url(r'^post_login$', login.post_login, name='post_login'),
]
 